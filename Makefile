#
# Makefile for csv-hasher
#

CHUNKSIZE   							= 64K
CHECK_LINES 							= 16
SAMPLE_ITERATIONS         = 1024
SAMPLE_ROWS_PER_ITERATION = 1024
TESTS       							= tests
COLNAME     							= id
SAMPLE      							= $(TESTS)/sample.csv
OUTPUT      							= $(TESTS)/output.csv

vendor:
	pipenv install

sample:
	@#bin/make-sample $(ITERATIONS)
	pipenv run ./csv-sampler.py --iterations $(SAMPLE_ITERATIONS) --rows_per_iteration $(SAMPLE_ROWS_PER_ITERATION) $(SAMPLE)

test-sample:
	pipenv run ./csv-hasher.py --check --chunksize $(CHUNKSIZE) $(SAMPLE) $(OUTPUT) $(COLNAME)

show-test-output:
	head -$(CHECK_LINES) $(SAMPLE)
	head -$(CHECK_LINES) $(OUTPUT)
	tail -$(CHECK_LINES) $(SAMPLE)
	tail -$(CHECK_LINES) $(OUTPUT)
	wc -l    						 $(SAMPLE)
	wc -l    						 $(OUTPUT)
	ls -lh   						 $(TESTS)

clean-sample:
	rm -f tests/*.csv

clean: clean-sample

test: clean-sample sample test-sample show-test-output
