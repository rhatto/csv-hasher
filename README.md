# csv-hasher

Hashes a given column of a CSV file.

## Requirements

Running:

* [Python 3](https://python.org).
* [Pandas](https://pandas.pydata.org).
* [tqdm](https://pypi.org/project/tqdm/).
* [python-humanfriendly](https://github.com/xolox/python-humanfriendly).

Testing:

* [GNU Make](https://www.gnu.org/software/make/).
* [Pipenv](https://pipenv.pypa.io).

## Testing

    make vendor
    make test

## Future

Some ideas:

* Submit package to PyPi.
* Multi-column processing.
* Optional hashing with salt.
* Support for more crypto functions other than hashing, such as symmetric encryption/decryption with,
  reading the passphrase from user input.
